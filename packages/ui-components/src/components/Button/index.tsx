import { ComponentProps } from "react";

export const Button = ({
  children,

  ...props
}: ComponentProps<"button">) => {
  return (
    <>
      <button className="text-3xl text-center font-bold" {...props}>
        {children}
      </button>
    </>
  );
};
