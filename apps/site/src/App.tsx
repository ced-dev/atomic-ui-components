import { QueryClient, QueryClientProvider } from "react-query";

import { Example } from "./react-query/Example";

const queryClient = new QueryClient();
export function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <Example />
    </QueryClientProvider>
  );
}
