import { Button } from "ui-components";

type Props = {
  todo: string;
  onComplete: () => void;
};
const TodoItem = ({ todo, onComplete }: Props) => {
  return (
    <li>
      {todo}
      <Button className="bg-brown-200" onClick={onComplete}>
        <span className="ml-4">Complete</span>
      </Button>
    </li>
  );
};

export default TodoItem;
