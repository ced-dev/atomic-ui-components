type Props = {
  newTodo: string;
  setNewTodo: (value: string) => void;
  addTodo: () => void;
};
export const TodoForm = ({ newTodo, setNewTodo, addTodo }: Props) => {
  return (
    <div>
      <input
        type="text"
        value={newTodo}
        onChange={(e) => setNewTodo(e.target.value)}
        placeholder="Enter a new todo"
      />
      <button onClick={addTodo}>Add Todo</button>
    </div>
  );
};
