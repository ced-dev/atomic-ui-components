import { useQuery } from "react-query";

const fetchData = async () => {
  const res = await fetch(import.meta.env.VITE_API_URL);
  if (!res.ok) {
    throw res.statusText;
  }
  return res.json();
};
export const useFetchData = () => {
  return useQuery({
    queryKey: "repoData",
    queryFn: fetchData,
  });
};
