import { useFetchData } from "./useQuery";

export const Example = () => {
  const { data, isLoading } = useFetchData();

  return (
    <div>
      <h1>React query test example </h1>
      <p>Topics : </p>
      {isLoading ? (
        <p>Loading data...</p>
      ) : (
        data.topics.map((topic: string) => <li key={topic}>{topic} </li>)
      )}
    </div>
  );
};
