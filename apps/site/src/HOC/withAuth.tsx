import { FunctionComponent } from "react";

const withAuth = (Component: FunctionComponent) => {
  return <Component />;
};
export default withAuth;
