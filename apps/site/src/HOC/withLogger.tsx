import { FunctionComponent, useEffect } from "react";

const withLogger = (Component: FunctionComponent) => {
  const WithLogger = (props: object) => {
    useEffect(() => {
      console.log(`Component ${Component.name} mounted`);
      return () => console.log(`Component ${Component.name} unmounted`);
    }, []);

    useEffect(() => {
      console.log(`Component ${Component.name} updated.`);
    });
    return <Component {...props} />;
  };
  //   WithLogger.displayName = `withLogger(${
  //     Component.displayName || Component.name
  //   })`;
  return WithLogger;
};
export default withLogger;
